#include <io.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
/*************************
 * eg:
 * FILEVERSION 1,0,0,0
   PRODUCTVERSION 1,0,0,0
 * VALUE "FileVersion", "1.0.0.0"
 * VALUE "ProductVersion", "1.0.0.0"
 *
****************************/

char* ModifyVersion(char* bufline, char split)
{
    char *version_num = strrchr(bufline, split) + 1;
    if(!version_num)
    {
        return NULL;
    }

    int number = atoi(version_num) + 1;
    char number_c[10] = {0};
    itoa(number, number_c, 10);
    strncpy(version_num, number_c, strlen(number_c));

    return bufline;
}

int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        return -1;
    }

    const char *rc_file_path = argv[1];     // rc_file_path = ****.rc
    if(access(rc_file_path, 0) < 0)
    {
        printf("the file is not exist, file_path: %s\r\n", rc_file_path);
        return -2;
    }

    FILE *pf = fopen(rc_file_path, "r");
    if(!pf)
    {
        printf("the file open failed, file_path: %s\r\n", rc_file_path);
    }

    char allbuf[1024] = {0};
    do{
        char buf[100] = {0};
        fgets(buf, 100, pf);
        if(strstr(buf, "FILEVERSION"))
        {
            ModifyVersion(buf, ',');
        }
        else if(strstr(buf, "PRODUCTVERSION"))
        {
            ModifyVersion(buf, ',');
        }
        else if(strstr(buf, "FileVersion"))
        {
            ModifyVersion(buf, '.');
        }
        else if(strstr(buf, "ProductVersion"))
        {
            ModifyVersion(buf, '.');
        }
        strcat(allbuf, buf);

    }while(!feof(pf));

    fclose(pf);

    pf = fopen(rc_file_path, "w");
    fwrite(allbuf, strlen(allbuf), 1, pf);
    fclose(pf);

    return 0;
}
