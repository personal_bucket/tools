#include "timermanager.h"

std::map<UINT_PTR, CTimerManager::Callback*> CTimerManager::_timers;

#pragma comment(lib, "user32");

void ThreadFun(void *param)
{
    CTimer *pTimer = (CTimer *)param;

    UINT_PTR timerId = CTimerManager::AddTimer(new CTimerManager::ClassCallback<CTimer>(pTimer, &CTimer::OnTimer)
                            , pTimer->getInterval());

    pTimer->setTimerId(timerId);
    MSG msg;

    while (GetMessage(&msg, NULL, NULL, NULL) && pTimer->isRunning())
    {
        if (msg.message == WM_TIMER)
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    CTimerManager::RemoveTimer(timerId);
}

CTimer::CTimer()
    : m_interval(100)
{

}

CTimer::~CTimer()
{
    m_bIsRuning = false;
}

void CTimer::startTimer(__int64 interval)
{
    m_interval = interval;
    _beginthread(ThreadFun, 0, this);
}

void CTimer::stopTimer()
{
    m_bIsRuning = false;
}

void CTimer::setTimerId(UINT_PTR timerId)
{
    m_timerId = timerId;
}

void CTimer::OnTimer(DWORD dwTime)
{

    //to do something
}
