#ifndef TIMERMANAGER_H
#define TIMERMANAGER_H

#include <Windows.h>
#include <process.h>
#include <map>

class CTimerManager
{
public:
    class Callback {
    public:
        virtual void operator()(DWORD dwTime) = 0;
    };
    template<class T>
    class ClassCallback : public Callback
    {
        T* _classPtr;
        typedef void(T::*fnTimer)(DWORD dwTime);
        fnTimer _timerProc;
    public:
        ClassCallback(T* classPtr, fnTimer timerProc) :_classPtr(classPtr), _timerProc(timerProc) {}
        virtual void operator()(DWORD dwTime) {
            (_classPtr->*_timerProc)(dwTime);
        }
    };

    static void RemoveTimer(UINT_PTR timerId)
    {
        KillTimer(NULL, timerId);
        _timers.erase(_timers.find(timerId));
    }

    static UINT_PTR AddTimer(Callback* timerObj, DWORD interval)
    {
        UINT_PTR id = SetTimer(NULL, 0, interval, TimerProc);
        // add the timer to the map using the id as the key
        _timers[id] = timerObj;
        return id;
    }
    static void CALLBACK TimerProc(HWND hwnd, UINT msg, UINT_PTR timerId, DWORD dwTime)
    {
        _timers[timerId]->operator()(dwTime);
    }
private:
    static std::map<UINT_PTR, Callback*> _timers;
};

class CTimer
{
public:
    CTimer();
    ~CTimer();
    void startTimer(__int64 interval);
    void stopTimer();
    void setTimerId(UINT_PTR timerId);

    const inline __int64 getInterval() const {
        return m_interval;
    }
    const inline bool isRunning() const {
        return m_bIsRuning;
    }

    virtual void OnTimer(DWORD dwTime);
private:
    bool m_bIsRuning;
    UINT_PTR m_timerId;
    __int64 m_interval;
};

#endif // TIMERMANAGER_H
